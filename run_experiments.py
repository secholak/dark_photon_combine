from ROOT import *
import math
import glob
import os
import numpy as np

param_scale = 1000000.

limits = {}
# This one is used to run a single bin experiments
# cms/workdir/eta2mumug/combine/combine_output/dpCard_Cat0_210000.txt
# cards = glob.glob('../combine_output/datacards/dpCard_parametric_*.txt')
# masses = []
# for card in cards:
#     print(card)
#     mass = card.split("dpCard_parametric_")[1]
#     mass = mass.rstrip(".txt")
#     mass = mass.lstrip("Cat")
#     mass = mass.replace("_", ".")
#     mass_float = float(mass)
#     masses.append(mass_float)

#     # os.system("combine -M AsymptoticLimits ../combine_output/datacards/" + card + " -m " + mass)
#     # os.system("combine -M AsymptoticLimits " + card + " -m " + mass + " --setParameterRanges a06=0.5,3.0 --rMax 10 ")
#     os.system("combine -M AsymptoticLimits " + card + " -m " + mass)
#     os.system("mv higgsCombineTest.AsymptoticLimits.mH"+str(mass_float)+".root ../combine_output/datacards/.")
#     f = TFile.Open("../combine_output/datacards/higgsCombineTest.AsymptoticLimits.mH"+str(mass_float)+".root","READ")
#     t = f.Get("limit")
    
#     t.GetEntry(2) # median expected limit
    
#     print("limit on (eps^2 * 1e6) for m(A')="+str(mass)+" GeV is",t.limit)
#     print("limit on eps for m(A')="+str(mass)+" GeV is",math.sqrt(t.limit/param_scale))

#     limits[mass] = math.sqrt(t.limit/param_scale)



# print(limits)


# This one is for a multibin experiment
# combineCards.py  Cat_0_400000_0=dpCard_parametric_Cat_0_400000_0.txt > combined_test.txt


cards_bin = glob.glob('../combine_output/datacards/dpCard_parametric_*_0.txt')
print("##"*45)
print(f"Found {len(cards_bin)} mass points for a single bin")


mass_start = 0.2400
mass_stop = 0.4700
mass_step = 0.0025

# n_bins = 24

bins = range(0,40)

n_mass_points = int((mass_stop - mass_start)/mass_step)

m_vals = np.arange(mass_start, mass_stop, mass_step)
m_vals = np.sort(m_vals)
# m_labels = [f"{m:.6f}" for m in m_vals]

cards_path = "../combine/combine_output/datacards/"

m_vals = [0.26000]
for m in m_vals:
    m = round(m,4)
    m_label = f"{m:.6f}"
    m_label = m_label.replace(".", "_")
    # bin_mass_cards = [ f"Cat_{m_label}_{i_bin}={cards_path}dpCard_parametric_Cat_{m_label}_{i_bin}.txt" for i_bin in range(n_bins)]
    bin_mass_cards = [ f"Cat_{m_label}_{i_bin}={cards_path}dpCard_parametric_Cat_{m_label}_{i_bin}.txt" for i_bin in bins]
    combined_cards = "combineCards.py  "
    for exp_bin in bin_mass_cards: combined_cards = combined_cards + exp_bin + " "
    combined_card_name = f"{cards_path}combined_Cat_{m_label}.txt"
    combined_cards += f" > {combined_card_name}"
    print(f"Making a combined datacard for mass={m} with n_bins={len(bins)}")
    print(combined_cards)
    os.system(combined_cards)
    os.system("combine -M AsymptoticLimits " + combined_card_name + " -m " + str(m) + " --rMax 3")
    os.system("mv higgsCombineTest.AsymptoticLimits.mH"+str(m)+".root ../combine_output/datacards/.")
    f = TFile.Open("../combine_output/datacards/higgsCombineTest.AsymptoticLimits.mH"+str(m)+".root","READ")
    t = f.Get("limit")
    
    t.GetEntry(2) # median expected limit
    
    print("limit on (eps^2 * 1e6) for m(A')="+str(m)+" GeV is",t.limit)
    print("limit on eps for m(A')="+str(m)+" GeV is",math.sqrt(t.limit/param_scale))

    limits[m] = math.sqrt(t.limit/param_scale)
    # break
# print(limits)
