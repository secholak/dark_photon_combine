imax * number of channels
jmax * number of background
kmax * number of nuisance parameters
kmax * number of nuisance parameters
shapes   data_obs	    Cat0_400000  dp_workspace_parametric_Cat0_400000.root  dpworkspace:data_obs
shapes  isEta2MuMu   Cat0_400000  dp_workspace_parametric_Cat0_400000.root dpworkspace:isEta2MuMu  
shapes  isEta2MuMuGamma   Cat0_400000  dp_workspace_parametric_Cat0_400000.root dpworkspace:isEta2MuMuGamma  
shapes  isOmega2Pi0MuMu   Cat0_400000  dp_workspace_parametric_Cat0_400000.root dpworkspace:isOmega2Pi0MuMu  
shapes  isKK2mumu   Cat0_400000  dp_workspace_parametric_Cat0_400000.root dpworkspace:isKK2mumu  
shapes  combinatorial   Cat0_400000  dp_workspace_parametric_Cat0_400000.root dpworkspace:combinatorial  
shapes  sig_model	    Cat0_400000 dp_workspace_parametric_Cat0_400000.root dpworkspace:sig_model
bin		Cat0_400000
observation     -1.0
bin     Cat0_400000   Cat0_400000   Cat0_400000   Cat0_400000   Cat0_400000   Cat0_400000   
process 		sig_model   isEta2MuMu   isEta2MuMuGamma   isOmega2Pi0MuMu   isKK2mumu   combinatorial
process 		0           1            2                 3                 4           5
rate    		1           1            1                 1                 1           1
sig_model_norm rateParam  Cat0_400000  sig_model  (@0*0.012371)  isEta2MuMu_norm
isEta2MuMu_norm  rateParam  Cat0_400000  isEta2MuMu   20051.887022
isEta2MuMuGamma_norm  rateParam  Cat0_400000  isEta2MuMuGamma   467873.848736
isOmega2Pi0MuMu_norm  rateParam  Cat0_400000  isOmega2Pi0MuMu   230725.880667
isKK2mumu_norm  rateParam  Cat0_400000  isKK2mumu   342674.780283
combinatorial_norm  rateParam  Cat0_400000  combinatorial   1152395.603292
