# Dark Photon Combine

Scripts to manage templates creation, fitting and integration into the combine datacards. It does also include code to run combine limits and plotting routines. Use combine v9, the recommended version (https://cms-analysis.github.io/HiggsAnalysis-CombinedLimit/latest/)

## MC templates
Run ```root -x make_templates.cpp``` to run over MC files located in ```inputFileName```, extract templates specified in ```selections``` from each file and to save merged template histograms into ```outputFileName```. This will create a Root file with the MC template histograms that are used for the fit to data.

## Combine datacards
There are several approaches already developed to run fits but only one is used. Executing this step will create datacards and ROOT files with workspaces.

### Parametric templates
Using parametric shapes to describe each background contribution. This is the main mode in which analysis is being developed now.
Run ```root -x make_parametric_dp_experiment.cpp``` for a single point, and call ```makeAllExperiments()``` to make datacards for all the masses.

Methods bellow are deprecated due to low statistics of the MC templates, which results into poor fit quality. However it was tested only in the integral fit, before splitting into `p_t` bins.
### RooHistPdf templates
Using RooFit RooHistPdf templates that have a fixed shape and free normalisation params, run ```root -x make_dp_experiments.cpp``` and call ```makeAllExperiments``` function with ```shapesType = "mixed"``` argument.

### TH1 templates
Using ROOT TH1 as a template shape for backgrounds and TH1 generated from the signal pdf, run ```root -x make_dp_experiments.cpp``` and call ```makeAllExperiments``` function with ```shapesType = "TH1"``` argument.


## Submitting jobs to Condor

Run `python3 submit_hunt_jobs.py ` to send either FitDiagnostics or AsymptoticLimit jobs.

## Running Combine locally and plotting results
Run ```python3 run_experiments.py``` to run Asymptotic limits and ```python3 plot_limits.py``` to plot them. Additionally, ```plot_postfit.py``` is developed for a point-by-point debugging of fit reuslts.

## Examples
A simple example of a parametric datacard and corresponding workspace is under ```examples/``` 
