#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooHistPdf.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include <cmath>

using namespace RooFit;


void plot_fit_result(){
    std::string dFileName = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/histosJan29.root";
    std::string dHistName = "massDimu_pt11to13_barrel_isMediumMuon";
    TFile* dFile = TFile::Open(dFileName.c_str());
    // Get the target histogram from the file
    TH1F* th1_tmp = dynamic_cast<TH1F*>(dFile->Get(dHistName.c_str()));
}
