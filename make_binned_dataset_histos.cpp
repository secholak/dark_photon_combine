#include <iostream>
#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <yaml-cpp/yaml.h>

#include "template_helpers.h"
#include "data_loaders.h"


// Function to check if a string contains a matching pattern
// needed to skipp already processed files
bool containsPattern(const std::string& str, const std::string& pattern) {
    std::regex regexPattern(pattern);
    return std::regex_search(str, regexPattern);
}


int make_binned_dataset_histos() {

    // Load the YAML configuration file
    YAML::Node config_ana = YAML::LoadFile("config_ana.yaml");

    std::string files_path = "/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/";
    std::string file_name_pattern = "mmgTree_";
    std::string outputFile ="/eos/user/s/secholak/CMS/eta2mumug/ntuples/run3_23/merged_binned_mumu_pt_data_histos.root";

    std::vector<std::string> files = getFilesList(files_path, file_name_pattern);

    Double_t mass_min = config_ana["hunt_parameters"]["mass_spectrum_min"].as<double>();
    Double_t mass_max = config_ana["hunt_parameters"]["mass_spectrum_max"].as<double>();
    Double_t bin_width = config_ana["hunt_parameters"]["mass_spectrum_bin_width"].as<double>();
    Int_t n_bins = (mass_max - mass_min)/bin_width;

    std::string hist_var = "mass";

    // Muon ID cuts that we might use later
    // muonID1/muonID2 are unfolded as ['isHighPtMuon', 'isLooseMuon', 'isMediumMuon', 'isSoftMuon', 'isTightMuon']
    // std::string base_selection = "(hltResult[0] == 1 || hltResult[1] == 1 ) && ((muonID1[2] == 1) && (muonID2[2] == 1)) && ( (pt > 11) && (pt < 13) )";
    // std::string base_selection = "(hltResult[0] == 1 || hltResult[1] == 1 )";

    // dummy cut
    std::string base_selection = "(mass > 0)";

    // list of bin cut for eta
    // Loading binning1_cuts
    std::vector<std::string> binning1_cuts;
    for (const auto& value : config_ana["hunt_parameters"]["eta_bins"]) {
        binning1_cuts.push_back(value.as<std::string>());
    }
    // Printing binning1_cuts
    std::cout << "Using the following binning1_cuts: " << std::endl;
    for (const auto& cut : binning1_cuts) {
        std::cout << cut << std::endl;
    }

    // Loading pt_bins
    std::vector<double> pt_bins;
    for (const auto& value : config_ana["hunt_parameters"]["pt_bins"]) {
        pt_bins.push_back(value.as<double>());
    }

    // Printing pt_bins
    std::cout << "Using the following pt_bins: ";
    for (const auto& pt : pt_bins) {
        std::cout << pt << " ";
    }
    std::cout << std::endl;

     // make a list of bin cuts for pt
    std::vector<std::string> binning2_cuts;
    for (int idx=0; idx < pt_bins.size() - 1; idx++){
        // leading muon pt
        // std::string cut = "(pt1 >= " + std::to_string(pt_bins[idx]) + " && pt1 < " + std::to_string(pt_bins[idx+1]) + " )"; 
        // dimuon pt
        std::string cut = "(pt >= " + std::to_string(pt_bins[idx]) + " && pt < " + std::to_string(pt_bins[idx+1]) + " )"; 
        binning2_cuts.push_back(cut);

    }

    std::vector<templateSel> selections;
    int bins_count = 0;
    for (const std::string& cut_1 : binning1_cuts){
        for (const std::string& cut_2 : binning2_cuts){
            templateSel sel;
            sel.name = std::to_string(bins_count);
            sel.selection = cut_1 + " && " + cut_2;
            selections.push_back(sel);
            std::cout << "Bin name:   " << sel.name << "   bin selection:   " << sel.selection  <<  std::endl;
            bins_count++;

        }
    }


     // List of input ROOT files containing histograms, to merge
    std::vector<std::string> templtHistoFiles;
    int f_count = 0;
    for (const std::string& f_path : files){
        if (containsPattern(f_path, "_histos")){
            continue;
        }
        std::cout << "processing file # " << f_count << std::endl;
        std::string out_f = f_path.substr(0, f_path.size() - 5) + "_histos.root";

        std::cout << f_path << std::endl;
        std::cout << out_f << std::endl;

        createHistograms(f_path.c_str(), out_f.c_str(), n_bins, mass_min, mas_max, hist_var, selections, base_selection);
        f_count++;
        templtHistoFiles.push_back(out_f);
    }
    std::cout<<"loaded # of files == "<< f_count << std::endl;
    

    // Output ROOT file for the merged histograms
    mergeHistograms(templtHistoFiles, outputFile);

    return 0;
}

