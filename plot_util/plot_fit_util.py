import ROOT, os, sys, yaml
import matplotlib.pyplot as plt
import numpy as np

import matplotlib
import matplotlib as mpl

ROOT.gSystem.AddIncludePath("-I$CMSSW_BASE/src/ ")
ROOT.gSystem.Load("$CMSSW_BASE/lib/$SCRAM_ARCH/libHiggsAnalysisCombinedLimit.so")
ROOT.gSystem.AddIncludePath("-I$ROOFITSYS/include")
ROOT.gSystem.AddIncludePath("-Iinclude/")

def computeChi2Pval(chi2_val, ndof):
	pval = ROOT.Math.chisquared_cdf_c(chi2_val, ndof)
	return pval

class FitPlot :
	mass_range = [0.2, 0.6]

	def __init__( self, name, x_title="m_{#mu#mu} [GeV]", y_title="Events / GeV", pad_height=0.24, ndivisions=505 ) :
		self.histos = []
		self.pull_histos = []
		width = 650;	height = 720
		left = 0.15;	right = 0.06
		top = 0.06;		bottom = 0.08
		middle = 0.03

		self.x_title = x_title
		self.y_title = y_title

		ROOT.gROOT.SetBatch()
		ROOT.gStyle.SetOptTitle(0)
		ROOT.gStyle.SetOptStat(0)
		ROOT.gStyle.SetPadTopMargin(0)
		ROOT.gStyle.SetPadRightMargin(right);		ROOT.gStyle.SetPadLeftMargin(left)
		ROOT.gStyle.SetNdivisions(ndivisions, "X");	ROOT.gStyle.SetNdivisions(ndivisions, "Y")
		ROOT.gStyle.SetTextSize(0.05)
		ROOT.gStyle.SetLabelSize(0.035)

		self.canvas = ROOT.TCanvas( "c_"+name, "c_"+name, width, height )
		self.canvas.SetFillStyle(4000)
		self.canvas.SetBottomMargin(pad_height+bottom);	self.canvas.SetTopMargin(top)
		self.canvas.cd()
		self.pad = ROOT.TPad( "pad_"+name, "pad_"+name,0,0,1,pad_height+bottom )
		self.pad.SetFillStyle(4000)
		self.pad.SetBottomMargin(bottom / pad_height);	self.pad.SetTopMargin(middle / pad_height)

	def nBins( self ) :
		return sum( [ self.data.GetBinContent(i) > 0 for i in range( self.data.GetNbinsX() + 1 ) ] )

	def chi2( self, pdf ) :
		chi2 = 0
		for i in range( 1, self.data.GetNbinsX()+1 ):
			if pdf.GetBinContent(i) != 0:
				chi2 += (pdf.GetBinContent(i)-self.data.GetBinContent(i))**2 / self.data.GetBinContent(i)
		return chi2

	def addData( self, data, title="Data", color=ROOT.kBlack, marker=ROOT.kFullCircle, marker_size=0.6 ) :
		self.canvas.cd()
		self.data = data
		if not self.data : return 0
		self.data.SetTitle(title)
		self.data.SetLineColor(color)
		self.data.SetMarkerStyle(marker)
		self.data.SetMarkerSize(marker_size)

		axes = ( self.data.GetXaxis(), self.data.GetYaxis() )
		axes[0].SetTitle( self.x_title )
		axes[0].SetRangeUser( self.mass_range[0], self.mass_range[1] )
		axes[0].SetLabelSize(0)
		axes[0].SetTitleSize(0)
		axes[1].SetTitle( self.y_title )
		axes[1].SetMaxDigits(2)
		axes[1].SetTitleOffset(1.7)
		axes[1].SetLabelSize(25)
		axes[1].SetTitleSize(25)
		for axis in axes :
			axis.SetLabelFont(43)
			axis.SetTitleFont(43)

		self.data.SetMinimum(0)
		self.data.Draw()
		self.canvas.Update()
		return self.data

	def addHistogram( self, histogram, title="", color=ROOT.kBlack, line_width=2 ) :
		if not histogram: return 0
		self.canvas.cd()
		self.histos.append( histogram )
		self.histos[-1].SetLineColor(color)
		self.histos[-1].SetLineWidth(line_width)
		if title : self.histos[-1].SetTitle(title)
		self.histos[-1].Draw("same hist")
		self.canvas.Update()
		return self.histos[-1]
	
	def addSumHistogram( self, histos, title="Sum", color=ROOT.kBlack, line_width=2 ) :
		if not histos: return 0
		sum_histo = histos[0].Clone(title)
		for h in histos[1:] :
			sum_histo.Add(h)
		return self.addHistogram( sum_histo, title, color, line_width )

	def addPullHistogram( self, histo, y_title="Pull", pad_ndivisions = 505 ) :
		if not histo or not self.data: return 0
		self.canvas.cd()
		self.pad.Draw("same")
		self.pad.cd()
		self.pull_histos.append( histo.Clone("ratio") )
		self.pull_histos[-1].Reset()
		for i in range( histo.GetNbinsX() ) :
			try : data_i = self.data.GetPointY(i)
			except AttributeError : data_i = self.data.GetBinContent(i+1)
			if data_i :
				self.pull_histos[-1].SetBinContent( i+1, (data_i - histo.GetBinContent(i+1)) / ROOT.TMath.Sqrt(data_i) )
				self.pull_histos[-1].SetBinError( i+1, ROOT.TMath.Sqrt(data_i) )
		axes = ( self.pull_histos[-1].GetXaxis(), self.pull_histos[-1].GetYaxis() )
		axes[0].SetTitle( self.x_title )
		axes[0].SetRangeUser( self.mass_range[0], self.mass_range[1] )
		axes[0].SetTitleOffset(3)
		axes[1].SetTitle( y_title )
		axes[1].SetTitleOffset(1.7)
		for axis in axes :
			axis.SetNdivisions(pad_ndivisions)
			axis.SetLabelSize(25)
			axis.SetLabelFont(43)
			axis.SetTitleFont(43)
			axis.SetTitleSize(25)
		self.pull_histos[-1].SetStats(0)
		self.pull_histos[-1].SetBarWidth(0.8)
		self.pull_histos[-1].SetFillColor( self.pull_histos[-1].GetLineColor() )
		self.pull_histos[-1].Draw("same hist b")
		self.pad.Update()
		return self.pull_histos[-1]

	def addLegend( self, header="", x1=0.52, y1=0.62, x2=0.93, y2=0.93 ) :
		self.canvas.cd()
		self.legend = ROOT.TLegend( x1, y1, x2, y2 )
		self.legend.Clear()
		self.legend.SetBorderSize(0)
		self.legend.SetTextSize(0.025)
		self.legend.SetFillStyle(0)
		if header : self.legend.SetHeader(header, "L")
		if self.data and self.data.GetTitle() :
			self.legend.AddEntry( self.data, self.data.GetTitle(), "P" )
		for histo in self.histos :
			if histo and histo.GetTitle() : self.legend.AddEntry( histo, histo.GetTitle(), "L" )
		self.legend.Draw("same")
		self.canvas.Update()
		return self.legend

	def addCMSText( self, extraText="Preliminary", lumi=0, energy=13.6 ) :
		self.canvas.cd()
		tag_CMS = ROOT.TLatex()
		tag_CMS.SetNDC()
		tag_CMS.SetTextAlign(11)
		tag_CMS.DrawLatex( self.canvas.GetLeftMargin()+0.05, 0.95-self.canvas.GetTopMargin(), "#scale[0.8]{CMS #bf{#it{"+extraText+"}}}")
		text_lumi = str(energy) + " TeV"
		if lumi > 0 : text_lumi = str(lumi)+" fb^{-1} ("+text_lumi+")"
		tag_lumi = ROOT.TLatex()
		tag_lumi.SetTextAlign(31)
		tag_lumi.SetNDC()
		tag_lumi.DrawLatex( 1-self.canvas.GetRightMargin(), 1.01-self.canvas.GetTopMargin(), "#scale[0.68]{#bf{"+text_lumi+"}}" )
		self.canvas.Update()

	def saveAs(self, out) :
		self.canvas.cd()
		canvas_line = ROOT.TLine(self.mass_range[0], 0, self.mass_range[1], 0)
		canvas_line.SetLineColor(ROOT.kBlack)
		canvas_line.SetLineWidth(1)
		canvas_line.Draw("same")
		self.canvas.Update()

		self.pad.cd()
		pad_line = ROOT.TLine(self.mass_range[0], 0, self.mass_range[1], 0)
		pad_line.SetLineColor(ROOT.kBlack)
		pad_line.SetLineWidth(1)
		pad_line.Draw("same")

		if self.histos :
			#self.data.SetMinimum( 1.1 * min(min([ h.GetMinimum() for h in self.histos if h != None ]), self.data.GetMinimum()) )
			#self.data.SetMaximum( 1.1 * max(max([ h.GetMaximum() for h in self.histos if h != None ]), self.data.GetMaximum()) )
			self.data.SetMinimum( 1.1 * min([ h.GetMinimum() for h in self.histos if h != None ]) )
			self.data.SetMaximum( 1.1 * max([ h.GetMaximum() for h in self.histos if h != None ]) )

		print( "Saving", out )
		self.canvas.SaveAs(out)

		for h in self.histos : del h


class FitDiagnosticsPlot(FitPlot) :

	def __init__( self, file, cat ) :
		self.cat = cat
		self.file = file
		super().__init__( "fitDiagnosticsPlot_"+cat )
		self.addData( self.file.Get("shapes_fit_s/"+ self.cat +"/data") )
		self.sb_fit = self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/total"), title="Signal + Background fit", color=ROOT.kRed )
		if self.sb_fit != None : self.addPullHistogram(self.sb_fit)
		self.b_fit = self.addHistogram( self.file.Get(f"shapes_fit_b/{cat}/total"), title="Background Only fit", color=ROOT.kBlue )
		if self.b_fit != None : self.addPullHistogram(self.b_fit)
		self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/sig_model"),			color=ROOT.kYellow+1,	title="Signal" )
		self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/combinatorial"),		color=ROOT.kGray,		title="Combinatorial" )
		self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/isEta2MuMu"),			color=ROOT.kGreen+3,	title="#eta#rightarrow#mu#mu" )
		self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/isEta2MuMuGamma"),	color=ROOT.kOrange+2, 	title="#eta#rightarrow#mu#mu#gamma" )
		self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/isKK2mumu"),			color=ROOT.kMagenta+2,	title="KK#rightarrow#mu#mu" )
		self.addHistogram( self.file.Get(f"shapes_fit_s/{cat}/isOmega2Pi0MuMu"),	color=ROOT.kAzure-3,	title="#omega#rightarrow#pi^{0}#mu#mu" )
		self.addLegend()
		self.addCMSText()

	def printParams( self, file_s="", file_b="" ) :
		headrow = ["PARAMETER", "INITIAL VALUE", "S+B FIT", "B-ONLY FIT"]
		headline = ("{:45} {:30} {:30} {:30}").format(*headrow)
		line = "".join(["-" for i in range(len(headline))])
		row_format = ("{:>45} {:>30} {:>30} {:>30}")
		print(line)
		print(headline)
		print(line)
		prefit = self.file.Get("norm_prefit")
		if prefit :
			print("NORMALIZATIONS:")
			fit_s = file_s.Get("norm_fit_s") if file_s else self.file.Get("norm_fit_s")
			fit_b = file_b.Get("norm_fit_b") if file_b else self.file.Get("norm_fit_b")
			iter = prefit.createIterator()
			while True :
				prefit_norm = iter.Next()
				if prefit_norm == None : break
				if self.cat not in prefit_norm.GetName() : continue
				name = prefit_norm.GetName()
				row = [
					name,
					"%10.3f +/- %-10.3f" % (prefit_norm.getVal(), prefit_norm.getError()),
					"%10.3f +/- %-10.3f" % (fit_s.find(name).getVal(), fit_s.find(name).getError()) if fit_s else "",
					"%10.3f +/- %-10.3f" % (fit_b.find(name).getVal(), fit_b.find(name).getError()) if fit_b else ""
				]
				print( row_format.format(*row) )

		print(line)
		fit_s = file_s.Get("fit_s") if file_s else self.file.Get("fit_s")
		fit_b = file_b.Get("fit_b") if file_b else self.file.Get("fit_b")
		float_pars_init = fit_b.floatParsInit() if fit_b else fit_s.floatParsInit() if fit_s else False
		if float_pars_init :
			print("FLOATING PARAMETERS:")
			iter = float_pars_init.createIterator()
			while True :
				par = iter.Next()
				if par == None : break
				name = par.GetName()
				if self.cat not in name : continue
				row = [
					name,
					"",
					"%10.3f +/- %-10.3f" % (fit_s.floatParsFinal().find(name).getVal(), fit_s.floatParsFinal().find(name).getError()) if fit_s else "",
					"%10.3f +/- %-10.3f" % (fit_b.floatParsFinal().find(name).getVal(), fit_b.floatParsFinal().find(name).getError()) if fit_b else ""
				]
				print( row_format.format(*row) )
		print(line)
		const_pars = fit_s.constPars() if fit_s else fit_b.constPars() if fit_b else False
		if const_pars :
			print("CONSTANT PARAMETERS:")
			iter = const_pars.createIterator()
			while True :
				par = iter.Next()
				if par == None : break
				name = par.GetName()
				if self.cat not in name : continue
				row = [
					name,
					"",
					"%10.3f +/- %-10.3f" % (fit_s.constPars().find(name).getVal(), fit_s.constPars().find(name).getError()) if fit_s else "",
					"%10.3f +/- %-10.3f" % (fit_b.constPars().find(name).getVal(), fit_b.constPars().find(name).getError()) if fit_b else ""
				]
				print( row_format.format(*row) )


class FitWorkspacePlot(FitPlot) :
	def __init__( self, ws, cat, v=False ) :
		self.cat = cat
		self.ws = ws
		sig_mass = float(cat.split("_")[2]) / 1000000
		super().__init__( "multiDimFitPlot_"+cat )
		var = ws.var("mass_obs")
		binning = ROOT.RooFit.Binning( var.getBinning().numBins(), var.getMin(), var.getMax() )
		channel = ROOT.RooFit.Cut(f"CMS_channel == CMS_channel::{cat}")

		data = ws.data("data_obs").reduce(channel)
		h_data = data.createHistogram( f"data_{cat}", var, binning, channel )
		h_data.Sumw2(0); h_data.Sumw2()	# recalculate Sumw2
		self.addData( h_data )

		self.model_s =	self.addWorkspaceHistogram( "model_s", var, binning, color=ROOT.kRed, title="Signal + Background Fit")
		self.model_s.Scale(h_data.Integral() / self.model_s.Integral())
		self.addPullHistogram(self.model_s)

		signal =		self.addWorkspaceHistogram( f"shapeSig_sig_model_{cat}", 		var, binning, norm=f"n_exp_bin{cat}_proc_sig_model",color=ROOT.kYellow+1, 	title=f"Signal ({sig_mass} GeV)" )
		combinatorial =	self.addWorkspaceHistogram( f"shapeBkg_combinatorial_{cat}",	var, binning, norm=f"combinatorial_{cat}_norm", 	color=ROOT.kGray, 		title="Combinatorial" )
		isEta2MuMu =	self.addWorkspaceHistogram( f"shapeBkg_isEta2MuMu_{cat}",		var, binning, norm=f"isEta2MuMu_{cat}_norm",		color=ROOT.kGreen+3, 	title="#eta#rightarrow#mu#mu"  )
		isEta2MuMuGamma=self.addWorkspaceHistogram( f"shapeBkg_isEta2MuMuGamma_{cat}",	var, binning, norm=f"isEta2MuMuGamma_{cat}_norm",	color=ROOT.kOrange+2, 	title="#eta#rightarrow#mu#mu#gamma"  )
		isKK2mumu =		self.addWorkspaceHistogram( f"shapeBkg_isKK2mumu_{cat}",		var, binning, norm=f"isKK2mumu_{cat}_norm", 		color=ROOT.kMagenta+2, 	title="KK#rightarrow#mu#mu"  )
		isOmega2Pi0MuMu=self.addWorkspaceHistogram( f"shapeBkg_isOmega2Pi0MuMu_{cat}",	var, binning, norm=f"isOmega2Pi0MuMu_{cat}_norm", 	color=ROOT.kAzure-3, 	title="#omega#rightarrow#pi^{0}#mu#mu"  )

		# signal =		self.addWorkspaceHistogram( f"shapeSig_sig_model_{cat}", 		var, binning, norm=f"sig_model_{cat}_norm" ,color=ROOT.kYellow+1, 	title=f"Signal ({sig_mass} GeV)" )
		# sum = self.addSumHistogram( [signal2, combinatorial, isEta2MuMu, isEta2MuMuGamma, isKK2mumu, isOmega2Pi0MuMu] )

		chi2 = self.chi2(self.model_s)
		ndof = self.nDof()
		pval = computeChi2Pval(chi2, ndof)
		self.addLegend(f"#chi2={chi2:3.1f}, P={pval:0.2e} for {ndof} nDoF")
		self.addCMSText()

	def addWorkspaceHistogram( self, name, var, binning, norm="", color=ROOT.kBlack, title="" ) :
		histo = self.addHistogram( self.ws.pdf(name).createHistogram(name, var, binning), color=color, title=title )
		if norm : histo.Scale( self.ws.function(norm).getValV() )
		return histo

	# Calculate number of nonconstant variables matching bin category
	#	If signal nonzero, adds 1 to total
	def nFreeParams( self, ws=None ) :
		if not ws : ws = self.ws
		nFreeParams = sum( [ self.cat in v.GetName() and not v.isConstant() for v in ws.allVars() ] )
		if not ws.arg("r").getVal() == 0 :
			nFreeParams += 1
		return nFreeParams

	def nDof( self, ws=None ) :
		if not ws : ws = self.ws
		return self.nBins() - self.nFreeParams( ws )
	
	def printParams( self, ws_prefit, ws_sb=None, ws_b=None ) :
		if not ws_sb and not ws_b : ws_sb = self.ws
		row_format = ("{:>45} {:>30} {:>30} {:>30}")
		headline = row_format.format(*["VARIABLE", "INITIAL VALUE", "S+B FIT", "B-ONLY FIT"])
		line = "".join(["-" for i in range(len(headline))])
		print(line)
		print(line)
		print(f"CATEGORY: {self.cat}")
		print(headline)
		print(line)
		var_list = ws_prefit.allVars() if ws_prefit else ( ws_sb.allVars() if ws_sb else ws_b.allVars() )
		rows = []
		for v in var_list :
			if self.cat not in v.GetName() : continue
			rows.append( [
				v.GetName(),
				ws_prefit.arg( v.GetName() ).getVal() if ws_prefit else 0,
				ws_prefit.arg( v.GetName() ).getError() if ws_prefit else 0,
				ws_sb.arg( v.GetName() ).getVal() if ws_sb else 0,
				ws_sb.arg( v.GetName() ).getError() if ws_sb else 0,
				ws_b.arg( v.GetName() ).getVal() if ws_b else 0,
				ws_b.arg( v.GetName() ).getError() if ws_b else 0
			])
		printRow = lambda row : print(row_format.format(*[row[0],"%10.3f +/- %-10.3f" % (row[1],row[2]),"%10.3f +/- %-10.3f" % (row[3],row[4]),"%10.3f +/- %-10.3f" % (row[5],row[6])]))
		for row in rows :
			if not row[1] == row[3] and not row[1] == row[5] :
				printRow(row)
		for row in rows :
			if row[1] == row[3] or row[1] == row[5] :
				printRow(row)
		print(line)
		print(row_format.format(*["FUNCTION", "INITIAL VALUE", "S+B FIT", "B-ONLY FIT"]))
		print(line)
		fn_list = ws_prefit.allFunctions() if ws_prefit else ( ws_sb.allFunctions() if ws_sb else ws_b.allFunctions() )
		for fn in fn_list :
			if self.cat not in fn.GetName() : continue
			row = [
				fn.GetName(),
				ws_prefit.function( fn.GetName() ).getValV() if ws_prefit else 0,
				ws_sb.function( fn.GetName() ).getValV() if ws_sb else 0,
				ws_b.function( fn.GetName() ).getValV() if ws_b else 0,
			]
			#printRow(row)
			


def makeFitDiagnosticsPlots() :
	with open('../config_ana.yaml', 'r') as file :
		config_ana = yaml.safe_load(file)
	condor_odir = config_ana['combine_parameters']['output_dir'] + "condor_sub/" + config_ana['combine_parameters']['condor_subdir']
	subfolders = [ f.path for f in os.scandir(condor_odir) if f.is_dir() ]
	for sf in subfolders[0:5] :
		for i in range(0,40) :
			cat = "Cat_0_" + str(sf.split("_")[-1]) + "_" + str(i)
			file = ROOT.TFile(sf + "/fitDiagnosticsTest.root")
			if not file : continue
			plot = FitDiagnosticsPlot(file, cat)
			if plot.data :
				plot.printParams()
				plot.saveAs(sf+"/plots/"+cat+".pdf")


def makeFitWorkspacePlots(method="MultiDimFit", v=False) :
	with open(sys.path[0] + '/../config_ana.yaml') as file :
		config_ana = yaml.safe_load(file)
	condor_odir = config_ana['combine_parameters']['output_dir'] + "condor_sub/" + config_ana['combine_parameters']['condor_subdir']
	subfolders = [ f.path for f in os.scandir(condor_odir) if f.is_dir() ]
	if v : print( "> Making plots in directory", condor_odir )
	for sf in subfolders :
		file_name = f"/higgsCombineTest.{method}.mH" + str( int(sf.split("_")[-1]) / 1000000 ) + ".root"
		file = ROOT.TFile(sf + file_name)
		if not file :
			if v : print( ">> could not open", file_name )
			continue
		elif v : print( ">> opened", file_name )
		ws = file.Get("w")
		ws.loadSnapshot(method)
		ws_prefit = file.Get("w")
		ws_prefit.loadSnapshot("clean")
		for i in range(0,40) :
			cat = "Cat_0_" + str(sf.split("_")[-1]) + "_" + str(i)
			if ws.cat("CMS_channel").hasLabel(cat) :
				if v : print(">>> using category", cat)
				ws.cat("CMS_channel").setIndex(i)
				plot = FitWorkspacePlot(ws, cat, v=True)
				plot.printParams(ws_prefit)
				plot.saveAs(sf+"/"+cat+f"_{method}.pdf")
				#corr = correlationMatrix( ROOT.TFile(sf+"/robustHesseTest.root"), sf+"/corr.pdf", cat )


def main() :
	#combineStatusTable()
	makeFitWorkspacePlots("MultiDimFit", v=True)
	#makeFitWorkspacePlots("AsymptoticLimits", v=True)

if __name__ == "__main__":
	main()

def test() :
	print("test")
	f = ROOT.TFile("fitDiagnosticsTest.root")
	f.Get("shapes_fit_s/Cat_0_325000_0/isKK2mumu")
	f.Print()
	p = FitDiagnosticsPlot(f, "Cat_0_325000_0")
	print(p.data)
	p.printParams()
	print("chi2", p.chi2(p.sb_fit))
	p.saveAs("test.pdf")