#ifndef DPHOTONRATE_H
#define DPHOTONRATE_H
#include <cmath>

class DPhotonRate {
private:
    TSpline3* br_gammaD2mumu;


public:
    Double_t Bgg  = 0.395;
    Double_t Bmmg = 0.00031;
    Double_t Bmm  = 0.0000058;
    Double_t MEta = 0.54786;
    // Constructor that reads data from a file
    DPhotonRate(const std::string& fileName, const std::string& splineName) {
        // Open the file
        std::ifstream infile(fileName);
        if (!infile.is_open()) {
            std::cerr << "Error: Unable to open file " + fileName << std::endl;
            return;
        }
        // Read the data from the file
        std::vector<double> xValues;
        std::vector<double> yValues;
        double x, y;

        while (infile >> x >> y) {
            xValues.push_back(x);
            yValues.push_back(y);
        }

        // Close the file
        infile.close();

        br_gammaD2mumu = new TSpline3("br_gammaD2mumu", &xValues[0], &yValues[0], xValues.size());

    }

   
    Double_t getRate(Double_t mass, std::string norm_channel, Double_t param_scale) const {
        Double_t rate;
        // ratio to norm channel
        Double_t sigscale;
        if (norm_channel.find("Eta2MuMuGamma") != std::string::npos){
            sigscale = 2.0*(Bgg/Bmmg)*pow(1.0-pow(mass/MEta,2),3);
        } 
        else {
            sigscale = 2.0*(Bgg/Bmm)*pow(1.0-pow(mass/MEta,2),3);
        }

        
        // form factor scale
        sigscale = sigscale*pow(1.0-1.83*pow(mass,2),-2);

        Double_t br = br_gammaD2mumu->Eval(mass);
        sigscale = sigscale * br;

       
       rate = sigscale/param_scale;
       return rate;
    }
};

#endif // DPHOTONRATE_H
